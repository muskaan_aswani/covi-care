<?php

namespace Database\Factories;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

class DoctorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Doctor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();
        $city = ['mumbai','pune','lucknow','delhi','kolkata','hyderabad','chennai','indore','ahemdabad'];
        $expertise = ['lungs','heart','neuro'];
        return [
            'name' => "Dr. ".$this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'expertise' => Arr::random($expertise, 1)[0],
            'phone' => rand(9000000001,9999999999),
            'experience' => rand(1,30),
            'work_name' => $faker->sentence(rand(1,4))." Hospital",
            'work_phone' =>  rand(9000000001,9999999999),
            'work_city' => Arr::random($city, 1)[0],
            'work_pincode' => rand(000001,999999),
            'status' => "pending"
        ];
    }
}
