<?php

namespace Database\Factories;

use App\Models\Hospital;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

class HospitalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Hospital::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();
        $city = ['mumbai','pune','lucknow','delhi','kolkata','hyderabad','chennai','indore','ahemdabad'];
        return [
            'name' => $faker->sentence(rand(1,4))." Hospital",
            'email' => $this->faker->unique()->safeEmail,
            'city' => Arr::random($city, 1)[0],
            'pincode' => rand(000001,999999),
            'phone' => rand(9000000001,9999999999),
            'max_doctors' => rand(10,100),
            'status' => "pending"
        ];
    }
}
