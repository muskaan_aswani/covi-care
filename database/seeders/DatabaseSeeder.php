<?php

namespace Database\Seeders;
use App\Doctor;
use App\Hospital;
use App\TeseCentre;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Doctor::factory(15)->create();
        \App\Models\TestCentre::factory(15)->create();
        \App\Models\Hospital::factory(15)->create();
        // factory(\App\Doctor::class,15)->create();
        // factory(\App\Testcentre::class,15)->create();
        // factory(\App\Hospital::class,15)->create();
    }
}
