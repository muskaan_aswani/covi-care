<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use App\Http\Requests\createDoctorRequest;
use App\Http\Requests\updateDoctorRequest;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i = 1;
        $sidebarSection = "doctor";
        $sidebarSubSection = "manage-doctor";
        $doctors = Doctor::where('status','pending')->orderBy('updated_at','DESC')->get();
        return view('dashboard.doctors.manage-doctor',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'doctors'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createDoctorRequest $request)
    {
        $doctor = Doctor::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'work_phone'=>$request->work_phone,
            'work_city'=>$request->work_city,
            'work_pincode'=>$request->work_pincode,
            'experience' => $request->experience,
            'expertise' => $request->expertise,
            'phone' => $request->phone,
            'work_name' => $request->work_name,
            'status' => "pending"
            
        ]);
        return redirect(route('home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        //
    }

    public function accepted($id)
    {
        $doctor = Doctor::where('id',$id)->get()[0];
        $doctor->status = "accepted";

        $doctor->save();

        session()->flash('success','Doctor Approved Successfully');
        return redirect()->back();
    
    }

    public function rejected($id)
    {
        
        $doctor = Doctor::where('id',$id)->get()[0];
        $doctor->status = "rejected";
        $doctor->save();
        session()->flash('danger','Doctor Rejected Successfully');
        return redirect(route('doctors.index'));
    }

    public function approvedDoctors(){
        $i = 1;
        $sidebarSection = "doctor";
        $sidebarSubSection = "approved-doctor";
        $doctors = Doctor::where('status','accepted')->orderBy('updated_at','DESC')->get();
        return view('dashboard.doctors.approved-doctor',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'doctors'
        ]));
    }

    public function rejectedDoctors(){
        $i = 1;
        $sidebarSection = "doctor";
        $sidebarSubSection = "rejected-doctor";
        $doctors = Doctor::where('status','rejected')->orderBy('updated_at','DESC')->get();
        return view('dashboard.doctors.rejected-doctor',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'doctors'
        ]));
    }
}

