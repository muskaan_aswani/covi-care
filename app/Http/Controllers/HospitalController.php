<?php

namespace App\Http\Controllers;

use App\Models\Hospital;
use Illuminate\Http\Request;
use App\Http\Requests\createHospitalRequest;
use App\Http\Requests\updateHospitalRequest;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i = 1;
        $sidebarSection = "hospital";
        $sidebarSubSection = "manage-hospital";
        $hospitals = Hospital::where('status','pending')->orderBy('updated_at','DESC')->get();
        return view('dashboard.hospitals.manage-hospital',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'hospitals'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createHospitalRequest $request)
    {
        $hospital = Hospital::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'city'=>$request->city,
            'pincode'=>$request->pincode,
            'max_doctors' => $request->max_doctors,
            'status' => "pending"
            
        ]);
        return redirect(route('home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function show(Hospital $hospital)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function edit(Hospital $hospital)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hospital $hospital)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hospital $hospital)
    {
        //
    }

    public function accepted($id)
    {
        $hospital = Hospital::where('id',$id)->get()[0];
        $hospital->status = "accepted";

        $hospital->save();

        session()->flash('success','Hospital Approved Successfully');
        return redirect()->back();
    
    }

    public function rejected($id)
    {
        
        $hospital = Hospital::where('id',$id)->get()[0];
        $hospital->status = "rejected";
        $hospital->save();
        session()->flash('danger','Hospital Rejected Successfully');
        return redirect(route('hospitals.index'));
    }

    public function approvedHospitals(){
        $i = 1;
        $sidebarSection = "hospital";
        $sidebarSubSection = "approved-hospital";
        $hospitals = Hospital::where('status','accepted')->orderBy('updated_at','DESC')->get();
        return view('dashboard.hospitals.approved-hospital',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'hospitals'
        ]));
    }

    public function rejectedHospitals(){
        $i = 1;
        $sidebarSection = "hospital";
        $sidebarSubSection = "rejected-hospital";
        $hospitals = Hospital::where('status','rejected')->orderBy('updated_at','DESC')->get();
        return view('dashboard.hospitals.rejected-hospital',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'hospitals'
        ]));
    }
}
