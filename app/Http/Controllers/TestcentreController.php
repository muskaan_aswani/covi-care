<?php

namespace App\Http\Controllers;

use App\Models\Testcentre;
use Illuminate\Http\Request;
use App\Http\Requests\createTestcentreRequest;
use App\Http\Requests\updateTestcentreRequest;

class TestcentreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i = 1;
        $sidebarSection = "tc";
        $sidebarSubSection = "manage-testcentre";
        $testcentres = Testcentre::where('status','pending')->orderBy('updated_at','DESC')->get();
        return view('dashboard.testcentres.manage-testcentre',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'testcentres'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createTestcentreRequest $request)
    {
        $testCentre = Testcentre::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'city'=>$request->city,
            'pincode'=>$request->pincode,
            'max_beds' => $request->max_beds,
            'max_staff' => $request->max_staff,
            'status' => "pending"
            
        ]);
        return redirect(route('home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Testcentre  $testcentre
     * @return \Illuminate\Http\Response
     */
    public function show(Testcentre $testcentre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testcentre  $testcentre
     * @return \Illuminate\Http\Response
     */
    public function edit(Testcentre $testcentre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Testcentre  $testcentre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testcentre $testcentre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testcentre  $testcentre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testcentre $testcentre)
    {
        //
    }

    public function accepted($id)
    {
        $testcentre = Testcentre::where('id',$id)->get()[0];
        $testcentre->status = "accepted";

        $testcentre->save();

        session()->flash('success','Test-Centre Approved Successfully');
        return redirect()->back();
    
    }

    public function rejected($id)
    {
        
        $testcentre = Testcentre::where('id',$id)->get()[0];
        $testcentre->status = "rejected";
        $testcentre->save();
        session()->flash('danger','Test-Centre Rejected Successfully');
        return redirect(route('testcentres.index'));
    }

    public function approvedTestcentres(){
        $i = 1;
        $sidebarSection = "tc";
        $sidebarSubSection = "approved-testcentre";
        $testcentres = Testcentre::where('status','accepted')->orderBy('updated_at','DESC')->get();
        return view('dashboard.testcentres.approved-testcentre',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'testcentres'
        ]));
    }

    public function rejectedTestcentres(){
        $i = 1;
        $sidebarSection = "tc";
        $sidebarSubSection = "rejected-testcentre";
        $testcentres = Testcentre::where('status','rejected')->orderBy('updated_at','DESC')->get();
        return view('dashboard.testcentres.rejected-testcentre',compact([
            'sidebarSection',
            'sidebarSubSection',
            'i',
            'testcentres'
        ]));
    }
}
