<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:50',
            'email'=>'required|unique:doctors,email,'. $this->doctor->id,
            'expertise'=> 'required',
            'phone' => 'required|max:15',
            'experience' => 'required',
            'work_name'=>'required|max:100',
            'work_phone'=>'required|max:15',
            'work_city'=>'required',
            'work_pincode'=>'required|max:6',
        ];
    }
}
