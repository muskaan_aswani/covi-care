<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateTestcentreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:50',
            'email'=>'required|unique:testcentres,email,'. $this->testcentre->id,
            'city'=>'required',
            'pincode'=> 'required|max:6',
            'phone' => 'required|max:15',
            'max_staff' => 'required',
            'max_beds' => 'required'
        ];
    }
}
