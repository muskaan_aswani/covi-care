@extends('dashboard.layout')
@section('title','Covi-Care | Hospitals')
@section('sub-title','Products')
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}">
    
@endsection
@section('main-content')

    <div class="modal fade"  id="delete-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	    <div>
				<div class="addmodal-container">
					<h1>Reject Hospital</h1><br>
				  <form action="" method="POST" id="delete-product" >
                      @csrf
                    <label for="">Are you sure you want to reject this hospital?</label>
                    <div class="row">
                        <div class="col-md-6">
                        <input type="submit"  id="delete" class="addmodal-submit" value="Reject">
                        </div>
                        <div class="col-md-6">
                            <input type="submit" class="addmodal-submit" id="cancel" value="Cancel">
                        </div>
                    </div>
				  </form>
				</div>
			</div>
    </div>
    <div class="card">
        <div class="card-header">Manage Hospitals</div>

        <div class="card-body">
            <table class="table table-bordered" id="manage-hospitals-table">
                <thead>
                    <th>#</th>   
                    <th>Name</th>
                    <th>Email</th>         
                    <th>Phone</th>
                    <th>City</th>
                    <th>Pin Code</th>
                    <th>Max Doctors</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($hospitals as $hospital)
                        <tr>
                            <td>
                                {{$i++}}
                            </td>
                            <td>
                                {{$hospital->name}}
                            </td>
                            <td>
                                {{$hospital->email}}
                            </td>

                            <td>
                                {{$hospital->phone}}
                            </td>

                            <td>
                                {{$hospital->city}}
                            </td>
                            <td>
                                {{$hospital->pincode}}
                            </td>
                            <td>
                                {{$hospital->max_doctors}}
                            </td>
                            
                            <td>
                                <a href="{{route('hospitals.accepted',$hospital->id)}}" class="btn btn-sm btn-primary">Accept</a>
                                
                                <a href="#"  class="btn btn-outline-primary btn-sm delete-trigger modal-trigger" data-toggle="modal" onclick="displayDeleteModalForm({{$hospital}})" data-target="#delete-modal">Reject</a>
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

          @endsection


          @section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
     <script>
         function displayDeleteModalForm($hospital) {
            var url = '/reject-hospital/'+$hospital.id;
           
            $("#delete-product").attr('action',url);
        }
        
        $(document).ready(function(){
            

             $("#cancel").click(function(e){
                 e.preventDefault();
                 $('#delete-modal').toggle();
                 
                 $('.modal-backdrop.show').toggle();
             });
                      
             
             $(".delete-trigger").click(function(e){
                 $('#delete-modal').toggle();
                 $('.modal-backdrop.show').toggle();
             });

             
         });
         $(document).ready(function() {
        $('#manage-hospitals-table').DataTable({
    
    "columnDefs": [
                {
                    'orderable': false,
                    'targets': [-1]
                }
            ]
        });
} );
     </script>
    
@endsection 