@extends('dashboard.layout')
@section('title','Covi-Care | Hospitals')
@section('sub-title','Products')
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}">
    
@endsection
@section('main-content')

    <div class="card">
        <div class="card-header">Rejected Hospitals</div>

        <div class="card-body">
            <table class="table table-bordered" id="manage-hospitals-table">
                <thead>
                    <th>#</th>   
                    <th>Name</th>
                    <th>Email</th>         
                    <th>Phone</th>
                    <th>City</th>
                    <th>Pin Code</th>
                    <th>Max Doctors</th>
                </thead>
                <tbody>
                    @foreach($hospitals as $hospital)
                        <tr>
                            <td>
                                {{$i++}}
                            </td>
                            <td>
                                {{$hospital->name}}
                            </td>
                            <td>
                                {{$hospital->email}}
                            </td>

                            <td>
                                {{$hospital->phone}}
                            </td>

                            <td>
                                {{$hospital->city}}
                            </td>
                            <td>
                                {{$hospital->pincode}}
                            </td>
                            <td>
                                {{$hospital->max_doctors}}
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

          @endsection


          @section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
     <script>
        
         $(document).ready(function() {
        $('#manage-hospitals-table').DataTable({
    
    "columnDefs": [
                {
                    'orderable': false,
                    'targets': [-1]
                }
            ]
        });
} );
     </script>
    
@endsection 