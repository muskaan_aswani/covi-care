<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  @yield('title')
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  
  <link href="{{asset('assets/css/material-dashboard.css?v=2.1.2')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('assets/datatables/datatables.min.css')}}">
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" integrity="sha256-46r060N2LrChLLb5zowXQ72/iKKNiw/lAmygmHExk/o=" crossorigin="anonymous" />


  @yield('page-level-styles')


</head>

<body class="">
  <div class="wrapper ">
    @include('dashboard.partials._sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboard.partials._navbar')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
        @include('dashboard.partials._message')
        @yield('main-content')
        </div>
      </div>
    
    </div>
    @include('dashboard.partials._footer')
  </div>
 @include('dashboard.partials._fixed-plugin')
  <!--   Core JS Files   -->
  <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  ></script>
  <script src="{{asset('assets/datatables/datatables.min.js')}}"></script>

    
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
  @yield('page-level-scripts')
</body>

</html>