@extends('dashboard.layout')
@section('title','Covi-Care | Doctors')
@section('sub-title','Products')
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}">
    
@endsection
@section('main-content')

    <div class="modal fade"  id="delete-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	    <div>
				<div class="addmodal-container">
					<h1>Reject Doctor</h1><br>
				  <form action="" method="POST" id="delete-product" >
                      @csrf
                    <label for="">Are you sure you want to reject this doctor?</label>
                    <div class="row">
                        <div class="col-md-6">
                        <input type="submit"  id="delete" class="addmodal-submit" value="Reject">
                        </div>
                        <div class="col-md-6">
                            <input type="submit" class="addmodal-submit" id="cancel" value="Cancel">
                        </div>
                    </div>
				  </form>
				</div>
			</div>
    </div>
    <div class="card">
        <div class="card-header">Manage Doctors</div>

        <div class="card-body">
            <table class="table table-bordered" id="manage-doctors-table">
                <thead>
                    <th>#</th>   
                    <th>Name</th>
                    <th>Expertise</th>  
                    <th>Experience</th>
                    <th>Email</th>         
                    <th>Phone</th>
                    <th>City</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($doctors as $doctor)
                        <tr>
                            <td>
                                {{$i++}}
                            </td>
                            <td>
                                {{$doctor->name}}
                            </td>
                            <td>
                                {{$doctor->expertise}}
                            </td>

                            <td>
                                {{$doctor->experience}}
                            </td>
                            <td>
                                {{$doctor->email}}
                            </td>

                            <td>
                                {{$doctor->phone}}
                            </td>

                            <td>
                                {{$doctor->work_city}}
                            </td> 

                            
                            <td>
                                <a href="{{route('doctors.accepted',$doctor->id)}}" class="btn btn-sm btn-primary">Accept</a>
                                
                                <a href="#"  class="btn btn-outline-primary btn-sm delete-trigger modal-trigger" data-toggle="modal" onclick="displayDeleteModalForm({{$doctor}})" data-target="#delete-modal">Reject</a>
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

          @endsection


          @section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
     <script>
         function displayDeleteModalForm($doctor) {
            var url = '/reject-doctor/'+$doctor.id;
           
            $("#delete-product").attr('action',url);
        }
        
        $(document).ready(function(){
            

             $("#cancel").click(function(e){
                 e.preventDefault();
                 $('#delete-modal').toggle();
                 
                 $('.modal-backdrop.show').toggle();
             });
                      
             
             $(".delete-trigger").click(function(e){
                 $('#delete-modal').toggle();
                 $('.modal-backdrop.show').toggle();
             });

             
         });
         $(document).ready(function() {
        $('#manage-doctors-table').DataTable({
    
    "columnDefs": [
                {
                    'orderable': false,
                    'targets': [-1]
                }
            ]
        });
} );
     </script>
    
@endsection 