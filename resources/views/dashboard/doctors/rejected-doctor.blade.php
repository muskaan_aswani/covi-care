@extends('dashboard.layout')
@section('title','Covi-Care | Doctors')
@section('sub-title','Products')
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}">
    
@endsection
@section('main-content')

    <div class="card">
        <div class="card-header">Rejected Doctors</div>

        <div class="card-body">
            <table class="table table-bordered" id="manage-doctors-table">
                <thead>
                    <th>#</th>   
                    <th>Name</th>
                    <th>Expertise</th>  
                    <th>Experience</th>
                    <th>Email</th>         
                    <th>Phone</th>
                    <th>City</th>
                    <th>Pincode</th>
                </thead>
                <tbody>
                    @foreach($doctors as $doctor)
                        <tr>
                            <td>
                                {{$i++}}
                            </td>
                            <td>
                                {{$doctor->name}}
                            </td>
                            <td>
                                {{$doctor->expertise}}
                            </td>

                            <td>
                                {{$doctor->experience}}
                            </td>
                            <td>
                                {{$doctor->email}}
                            </td>

                            <td>
                                {{$doctor->phone}}
                            </td>

                            <td>
                                {{$doctor->work_city}}
                            </td>

                            <td>
                                {{$doctor->work_pincode}}
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

          @endsection


          @section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
    <script>
        $(document).ready(function() {
        $('#manage-doctors-table').DataTable({
    
    "columnDefs": [
                {
                    'orderable': false,
                    'targets': [-1]
                }
            ]
        });
} );
     </script>
@endsection 