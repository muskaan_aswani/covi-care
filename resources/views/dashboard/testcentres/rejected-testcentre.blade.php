@extends('dashboard.layout')
@section('title','Covi-Care | Test-Centres')
@section('sub-title','Products')
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}">
    
@endsection
@section('main-content')

    <div class="card">
        <div class="card-header">Rejected Test-Centres</div>

        <div class="card-body">
            <table class="table table-bordered" id="manage-testcentres-table">
                <thead>
                    <th>#</th>   
                    <th>Name</th>
                    <th>Email</th>         
                    <th>Phone</th>
                    <th>City</th>
                    <th>Pin Code</th>
                    <th>Max Staff</th>
                    <th>Max Beds</th>
                </thead>
                <tbody>
                    @foreach($testcentres as $testcentre)
                        <tr>
                            <td>
                                {{$i++}}
                            </td>
                            <td>
                                {{$testcentre->name}}
                            </td>
                            <td>
                                {{$testcentre->email}}
                            </td>

                            <td>
                                {{$testcentre->phone}}
                            </td>

                            <td>
                                {{$testcentre->city}}
                            </td>
                            <td>
                                {{$testcentre->pincode}}
                            </td>
                            <td>
                                {{$testcentre->max_staff}}
                            </td>
                            <td>
                                {{$testcentre->max_beds}}
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

          @endsection


          @section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
     <script>
        
         $(document).ready(function() {
        $('#manag-testcentres-table').DataTable({
    
    "columnDefs": [
                {
                    'orderable': false,
                    'targets': [-1]
                }
            ]
        });
} );
     </script>
    
@endsection 