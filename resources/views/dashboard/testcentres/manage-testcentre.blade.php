@extends('dashboard.layout')
@section('title','Covi-Care | Test-Centres')
@section('sub-title','Products')
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/modal.css')}}">
    
@endsection
@section('main-content')

    <div class="modal fade"  id="delete-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	    <div>
				<div class="addmodal-container">
					<h1>Reject Test-Centre</h1><br>
				  <form action="" method="POST" id="delete-product" >
                      @csrf
                    <label for="">Are you sure you want to reject this test centre?</label>
                    <div class="row">
                        <div class="col-md-6">
                        <input type="submit"  id="delete" class="addmodal-submit" value="Reject">
                        </div>
                        <div class="col-md-6">
                            <input type="submit" class="addmodal-submit" id="cancel" value="Cancel">
                        </div>
                    </div>
				  </form>
				</div>
			</div>
    </div>
    <div class="card">
        <div class="card-header">Manage Test-Centres</div>

        <div class="card-body">
            <table class="table table-bordered" id="manage-testcentres-table">
                <thead>
                    <th>#</th>   
                    <th>Name</th>
                    <th>Email</th>         
                    <th>Phone</th>
                    <th>City</th>
                    <th>Pin Code</th>
                    <th>Max Staff</th>
                    <th>Max Beds</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($testcentres as $testcentre)
                        <tr>
                            <td>
                                {{$i++}}
                            </td>
                            <td>
                                {{$testcentre->name}}
                            </td>
                            <td>
                                {{$testcentre->email}}
                            </td>

                            <td>
                                {{$testcentre->phone}}
                            </td>

                            <td>
                                {{$testcentre->city}}
                            </td>
                            <td>
                                {{$testcentre->pincode}}
                            </td>
                            <td>
                                {{$testcentre->max_staff}}
                            </td>
                            <td>
                                {{$testcentre->max_beds}}
                            </td>
                            
                            <td>
                                <a href="{{route('testcentres.accepted',$testcentre->id)}}" class="btn btn-sm btn-primary">Accept</a>
                                
                                <a href="#"  class="btn btn-outline-primary btn-sm delete-trigger modal-trigger" data-toggle="modal" onclick="displayDeleteModalForm({{$testcentre}})" data-target="#delete-modal">Reject</a>
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>

          @endsection


          @section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
     <script>
         function displayDeleteModalForm($testcentre) {
            var url = '/reject-testcentre/'+$testcentre.id;
           
            $("#delete-product").attr('action',url);
        }
        
        $(document).ready(function(){
            

             $("#cancel").click(function(e){
                 e.preventDefault();
                 $('#delete-modal').toggle();
                 
                 $('.modal-backdrop.show').toggle();
             });
                      
             
             $(".delete-trigger").click(function(e){
                 $('#delete-modal').toggle();
                 $('.modal-backdrop.show').toggle();
             });

             
         });
         $(document).ready(function() {
        $('#manag-testcentres-table').DataTable({
    
    "columnDefs": [
                {
                    'orderable': false,
                    'targets': [-1]
                }
            ]
        });
} );
     </script>
    
@endsection 