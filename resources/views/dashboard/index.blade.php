@extends('dashboard.layout')    
@section('title','Covi-Care | Dashboard')


@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
@endsection
@include('dashboard.partials._message')
@section('main-content')
    @include('dashboard.partials._main-content')
@endsection

@section('page-level-scripts')
    @include('dashboard.partials._main-scripts')
@endsection