<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <div class="logo"><a href="{{route('home')}}" class="simple-text logo-normal">
         Covi Care
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
        <li class="nav-item {{$sidebarSection=='dashboard' ? 'active' : ''}}">
          <a class="nav-link" href="{{ route('dashboard.index')}}">
              <i class="fas fa-tachometer-alt"></i>
              <span>Dashboard</span></a>
          </li>


           <li class="nav-item {{ $sidebarSection =='doctor' ? 'active' : ''}}">
              <a class="nav-link collapsed" 
                  href="" data-toggle="collapse" 
                  data-target="#collapseDoctor" 
                  aria-expanded="true" 
                  aria-controls="collapseDoctor">
                  <i class="fas fa-user-md"></i>
                  <span>Doctor</span>
              </a>
              <div id="collapseDoctor" 
                  class="collapse {{ $sidebarSection=='doctor' ? 'show' : ''}}" 
                  aria-labelledby="headingTasks" 
                  data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                  <a class="collapse-item {{$sidebarSubSection=='manage-doctor' ? 'active' : ''}}" 
                      href="{{ route('doctors.index')}}">Manage Doctors</a>
                    <a class="collapse-item {{$sidebarSubSection=='approved-doctor' ? 'active' : ''}}" 
                      href="{{ route('doctors.showApproved')}}">Approved Doctors</a>
                  <a class="collapse-item {{$sidebarSubSection=='rejected-doctor' ? 'active' : ''}}" 
                      href="{{ route('doctors.showRejected')}}">Rejected Doctors</a>             
              </div>

        </li>
        
        <li class="nav-item {{ $sidebarSection =='hospital' ? 'active' : ''}}">
              <a class="nav-link collapsed" 
                  href="" data-toggle="collapse" 
                  data-target="#collapseHospital" 
                  aria-expanded="true" 
                  aria-controls="collapseHospital">
                  <i class="fas fa-hospital-o"></i>
                  <span>Hospital</span>
              </a>
              <div id="collapseHospital" 
                  class="collapse {{ $sidebarSection=='hospital' ? 'show' : ''}}" 
                  aria-labelledby="headingTasks" 
                  data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                  <a class="collapse-item {{$sidebarSubSection=='manage-hospital' ? 'active' : ''}}" 
                      href="{{ route('hospitals.index')}}">Manage Hospitals</a>
                    <a class="collapse-item {{$sidebarSubSection=='approved-hospital' ? 'active' : ''}}" 
                      href="{{ route('hospitals.showApproved')}}">Approved Hospitals</a>
                  <a class="collapse-item {{$sidebarSubSection=='rejected-hospital' ? 'active' : ''}}" 
                      href="{{ route('hospitals.showRejected')}}">Rejected Hospitals</a>                  
              </div>

        </li>

        <li class="nav-item {{ $sidebarSection =='tc' ? 'active' : ''}}">
              <a class="nav-link collapsed" 
                  href="" data-toggle="collapse" 
                  data-target="#collapsetc" 
                  aria-expanded="true" 
                  aria-controls="collapsetc">
                  <i class="fas fa-plus-square"></i>
                  <span>Test Centre</span>
              </a>
              <div id="collapsetc" 
                  class="collapse {{ $sidebarSection=='tc' ? 'show' : ''}}" 
                  aria-labelledby="headingTasks" 
                  data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                  <a class="collapse-item {{$sidebarSubSection=='manage-testcentre' ? 'active' : ''}}" 
                      href="{{ route('testcentres.index')}}">Manage Test-Centres</a>
                    <a class="collapse-item {{$sidebarSubSection=='approved-testcentre' ? 'active' : ''}}" 
                      href="{{ route('testcentres.showApproved')}}">Approved Test-Centres</a>
                  <a class="collapse-item {{$sidebarSubSection=='rejected-testcentre' ? 'active' : ''}}" 
                      href="{{ route('testcentres.showRejected')}}">Rejected Test-Centres</a>             
              </div>

        </li>

 

        </ul>
      </div>
    </div>