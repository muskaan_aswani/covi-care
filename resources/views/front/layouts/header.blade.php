<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html">COVI-Care</a></h1>
    

      <nav class="nav-menu d-none d-lg-block">
        <ul>
              
        
          <li class="drop-down"><a href="">For Providers</a>
            <ul>
             
              <li><a href="#">Register as a Hospital</a></li>
              <li><a href="#">Register as a Doctor</a></li>
              <li><a href="#">Register as a Test Center</a></li>
            </ul>
          </li>
            
        

        </ul>
      </nav><!-- .nav-menu -->

      <a href="#appointment" class="appointment-btn scrollto">Login/SignUp</a>

    </div>
  </header><!-- End Header -->
  