<html>
<title>registration Form</title>
<body>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Covi-Care</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

   <!-- Favicons -->
   <link href="{{asset('assetsfe/assets/img/favicon.png')}}" rel="icon">
  <link href="{{asset('assetsfe/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('assetsfe/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('assetsfe/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assetsfe/assets/css/style.css')}}" rel="stylesheet">

    

<style>
*, *:before, *:after {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body {
  font-family: 'Nunito', sans-serif;
  color: #384047;
}

form {
  max-width: 300px;
  margin: 100px auto;
  padding: 10px 20px;
  background: #f4f7f8;
  border-radius: 8px;
}

h1 {
  margin: 0 0 15px 0;
  text-align: center;
}

input[type="text"],
input[type="password"],
input[type="date"],
input[type="datetime"],
input[type="email"],
input[type="number"],
input[type="search"],
input[type="tel"],
input[type="time"],
input[type="url"],
textarea,
select {
  background: rgba(255,255,255,0.1);
  border: none;
  font-size: 16px;
  height: auto;
  margin: 0;
  outline: 0;
  padding: 5px;
  width: 100%;
  background-color: #e8eeef;
  color: #8a97a0;
  box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
  margin-bottom: 10px;
}

select {
  padding: 6px;
  height: 32px;
  border-radius: 2px;
}

button {
  padding: 19px 39px 18px 39px;
  color: #F9f9f9;
  background-color: #4bc970;
  font-size: 18px;
  text-align: center;
  font-style: normal;
  border-radius: 5px;
  width: 100%;
  border: 1px solid #3ac162;
  border-width: 1px 1px 3px;
  box-shadow: 0 -1px 0 rgba(255,255,255,0.1) inset;
  margin-bottom: 10px;
}

fieldset {
  margin-bottom: 10px;
  border: none;
}

legend {
  font-size: 1.2em;
  margin-bottom: 3px;
  color:blue;
}

label {
  display: block;
  margin-bottom: 2px;
}

label.light {
  font-weight: 300;
  display: inline;
}

.number {
  background-color: #5fcf80;
  color: #fff;
  height: 25px;
  width: 30px;
  display: inline-block;
  font-size: 0.8em;
  margin-right: 4px;
  line-height: 20px;
  text-align: center;
  text-shadow: 0 1px 0 rgba(255,255,255,0.2);
  border-radius: 100%;
}

@media screen and (min-width: 480px) {

  form {
    max-width: 480px;
  }

}
</style>
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="{{route('home')}}">COVI-Care</a></h1>
    

      <nav class="nav-menu d-none d-lg-block">
        <ul>
              
        
          <li class="drop-down"><a href="">For Providers</a>
          <ul>
             
             <li><a href="{{route('hospital_form')}}">Register as a Hospital</a></li>
             <li><a href="{{route('signup')}}">Register as a User</a></li>
             <li><a href="{{route('testcentre_form')}}">Register as a Test Center</a></li>
           </ul>
          </li>
            
        

        </ul>
      </nav><!-- .nav-menu -->

      <a href="{{route('signin')}}" class="appointment-btn scrollto">Login/SignUp</a>

    </div>
  </header><!-- End Header -->
  
      <form action="{{route('doctor.store')}}" method="POST">
        @csrf
        <h1>Register As A Doctor</h1>
        
        <fieldset>
          
          <label for="name">Full Name:</label>
          <input type="text" id="name" name="name"  class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"  placeholder="Full Name">

          @error('name')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
          
          <label for="mail">Email:</label>
          <input type="email" id="email" name="email"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"  placeholder="Email">

          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
          
          <label for="phone">Phone:</label>
          <input type="text" id="phone" name="phone"  class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}"  placeholder="Phone">

          @error('phone')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror

          <label for="experience">Field of Experience:</label>
          <input type="number" id="experience" name="experience"  class="form-control @error('experience') is-invalid @enderror" value="{{ old('experience') }}"  placeholder="Experience">

          @error('experience')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror

          <label for="expertise">Expertise:</label>
          <select id="expertise" name="expertise" class="form-control @error('expertise') is-invalid @enderror" value="{{ old('expertise') }}"> 
          <optgroup label="Choose A Specialization">
          <option value="lungs">Lungs</option>
            <option value="neuro">Neuro</option>
            <option value="heart">Heart</option>
     </optgroup></select>
    
          @error('expertise')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror

          </fieldset>
        
        <fieldset>
          <legend><span class="number">2</span>Your Work Profile</legend>
          <label>Clinic/Hospital Name:</label>
          <input type="text" id="work_name" name="work_name"  class="form-control @error('work_name') is-invalid @enderror" value="{{ old('work_name') }}"  placeholder="Clinic/Hospital Name">

          @error('work_name')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror

		  
		  <label for="work_city">Clinic/Hospital City:</label>
            <select id="work_city" name="work_city" class="form-control @error('work_city') is-invalid @enderror" value="{{ old('work_city') }}"> 
          <optgroup label="Choose A City">
          <option value="mumbai">Mumbai</option>
            <option value="pune">Pune</option>
            <option value="delhi">Delhi</option>
            <option value="kolkata"> Kolkata</option>
            <option value="hyderabad">Hyderabad</option>
            <option value="chennai">Chennai</option>
            <option value="indore">Indore</option>
            <option value="ahemdabad">Ahemdabad</option>
            <option value="lucknow">Lucknow</option>
     </optgroup></select>
     
         @error('work_city')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
		  	  
			   <label >Clinic/Hospital Pin Code: </label>
         <input type="number" id="work_pincode" name="work_pincode"  class="form-control @error('work_pincode') is-invalid @enderror" value="{{ old('work_pincode') }}"  placeholder="Pincode">

          @error('work_pincode')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
		  
          
          <label for="work_phone">Clinic/Hospital Phone:</label>
          <input type="text" id="work_phone" name="work_phone"  class="form-control @error('work_phone') is-invalid @enderror" value="{{ old('work_phone') }}"  placeholder="Phone">

          @error('work_phone')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
		  
        </fieldset>
         	  
		 <button type="submit">Register</button>
      </form>
<!-- ======= Footer ======= -->
<footer id="footer">

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3>Covi-Care</h3>
        <p>
          A108 Adam Street <br>
          New York, NY 535022<br>
          United States <br><br>
          <strong>Phone:</strong> +1 5589 55488 55<br>
          <strong>Email:</strong> info@example.com<br>
        </p>
      </div>

      <div class="col-lg-2 col-md-6 footer-links">
        <h4>About Us</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Our Services</h4>
      
      </div>

      <div class="col-lg-4 col-md-6 footer-newsletter">
        <h4>Subscribe For Daily Updates</h4>
       
        <form action="" method="post">
          <input type="email" name="email"><input type="submit" value="Subscribe">
        </form>
      </div>

    </div>
  </div>
</div>

<div class="container d-md-flex py-4">


  <div class="social-links text-center text-md-right pt-3 pt-md-0">
    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->
<!-- Vendor JS Files -->
<script src="{{asset('assetsfe/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assetsfe/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assetsfe/assets/js/main.js')}}"></script>
      
</body>
</html>
			
			