<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestcentreController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HospitalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('doctor', DoctorController::class);
Route::resource('hospital', HospitalController::class);
Route::resource('testcentre', TestcentreController::class);
// Route::get('/testcentres',[TestController::class, 'index']);
Route::get('/', function () {
    return view('front.index');
});
Route::get('/dashboard', function(){ 
    $sidebarSection = "dashboard";
    $sidebarSubSection ="";
    return view('dashboard.index',compact([
            'sidebarSection',
            'sidebarSubSection'
        ])); 
 })->name('dashboard.index');

Route::get('/manage-doctors', [DoctorController::class , 'index'])->name('doctors.index');
Route::get('/approved-doctors', [DoctorController::class , 'approvedDoctors'])->name('doctors.showApproved');
Route::get('/rejected-doctors', [DoctorController::class , 'rejectedDoctors'])->name('doctors.showRejected');
Route::get('/accept-doctor/{id}', [DoctorController::class , 'accepted'])->name('doctors.accepted');
Route::post('/reject-doctor/{id}', [DoctorController::class , 'rejected'])->name('doctors.rejected');

Route::get('/manage-hospitals', [HospitalController::class , 'index'])->name('hospitals.index');
Route::get('/approved-hospitals', [HospitalController::class , 'approvedhospitals'])->name('hospitals.showApproved');
Route::get('/rejected-hospitals', [HospitalController::class , 'rejectedhospitals'])->name('hospitals.showRejected');
Route::get('/accept-hospital/{id}', [HospitalController::class , 'accepted'])->name('hospitals.accepted');
Route::post('/reject-hospital/{id}', [HospitalController::class , 'rejected'])->name('hospitals.rejected');

Route::get('/manage-testcentres', [TestcentreController::class , 'index'])->name('testcentres.index');
Route::get('/approved-testcentres', [TestcentreController::class , 'approvedtestcentres'])->name('testcentres.showApproved');
Route::get('/rejected-testcentres', [TestcentreController::class , 'rejectedtestcentres'])->name('testcentres.showRejected');
Route::get('/accept-testcentre/{id}', [TestcentreController::class , 'accepted'])->name('testcentres.accepted');
Route::post('/reject-testcentre/{id}', [TestcentreController::class , 'rejected'])->name('testcentres.rejected');


Route::get('/article1', function () { return view('front.article1'); })->name('article1');
Route::get('/article2', function () { return view('front.article2'); })->name('article2');
Route::get('/article3', function () { return view('front.article3'); })->name('article3');
Route::get('/article4', function () { return view('front.article4'); })->name('article4');
Route::get('/article5', function () { return view('front.article5'); })->name('article5');

Route::get('/registerDoctor', function () { return view('front.doctor_form'); })->name('doctor_form');
Route::get('/registerHospital', function () { return view('front.hosp_form'); })->name('hospital_form');
Route::get('/registerTestCentre', function () { return view('front.testcnt_form'); })->name('testcentre_form');



Auth::routes();
Route::get('/signin', function () { 
    if(auth())
    return view('front.login'); 
    else
    return redirect(route('home'));
})->name('signin');
Route::get('/signup', function () { 
    if(auth())
    return view('front.register'); 
    else
    return redirect(route('home'));
 })->name('signup');

Route::get('/home', function () { return view('front.index'); })->name('home');

